package systemtest;

/**
 * Token object class
 * 
 * @author Anna Nguyen s154230
 *
 */

public class Token {
	private String string;
	private boolean used;
	
	public Token() {
		
	}
	
	public Token(String uniqueString) {
		this.string = uniqueString;
	}
	
	/**
	 * This method retrieves the string of a token.
	 * 
	 * @return the unique string associated with the token
	 */
	
	public String getString() {
		return this.string;
	}
	
	/**
	 * This method returns whether a token is used or not.
	 * 
	 * @return a boolean indicating whether the token has been used or not
	 */
	
	public boolean isUsed() {
		return used;
	}
	
	/**
	 * This method sets the boolean field of the token.
	 * The method is called when a token is scanned.
	 */
	
	public void use() {
		used = true;
	}
}
