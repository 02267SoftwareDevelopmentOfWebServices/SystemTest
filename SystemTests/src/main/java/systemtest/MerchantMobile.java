package systemtest;

import java.math.BigDecimal;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Merchant Mobile simulator
 * 
 * @author Ole Eilgaard s154086
 *
 */

public class MerchantMobile {
	Client client = ClientBuilder.newClient();
	WebTarget r;
	JSONArray tokenValues;
	String cpr;
	String result;
	String firstName;
	String lastName;

	public MerchantMobile(String cpr, String firstName, String lastName) {
		this.cpr = cpr;
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	/**
	 * This method simulates that the merchant connects to their bank account.
	 * 
	 * @return the string "true" if the merchant now has a bank account
	 */
	public String connectAccount() {
		r = client.target("http://02267-istanbul.compute.dtu.dk:8081/accounts");
		// Simulates the customer inputting personal information
		JSONObject info = new JSONObject();
		info.put("firstName", firstName);
		info.put("lastName", lastName);
		info.put("cpr", cpr);
		info.put("type", "merchant");
		
		String message = info.toString();
		
		String result = r.request().post(Entity.entity(message,"text/plain"),String.class);
		return result;
	}
	
	/**
	 * This method simulates that the merchant scans a token.
	 * 
	 * @param tokenId is the id of the token to be scanned
	 * @param amount is the amount of money to be transferred
	 * @return a string "true" if the transaction succeeds.
	 */

	public String scanToken(String tokenId, int amount) {
		r = client.target("http://02267-istanbul.compute.dtu.dk:8082/tokens/"+tokenId);

		result = r.request().post(null, String.class);

		if (result.equals("Token used")) {
			// Get owner of tokenid
			r = client.target("http://02267-istanbul.compute.dtu.dk:8082/tokens/"+tokenId);
			String theirCpr = r.request().get(String.class);

			// Transaction
			r = client.target("http://02267-istanbul.compute.dtu.dk:8083/payment/");

			Transaction trans = new Transaction();
			trans.setAmount(new BigDecimal(amount));
			trans.setCustomer(theirCpr);
			trans.setMerchant(cpr);

			Response response = r.request().post(Entity.entity(trans,"application/json"));
			result = response.readEntity(String.class);
			return result;
		}

		return result;
	}
	
	/**
	 * This method simulates that the merchant disconnects their bank account
	 * 
	 * @return the string "true" if disconnected
	 */
	public String disconnectAccount() {
		r = client.target("http://02267-istanbul.compute.dtu.dk:8081/accounts/"+cpr);

		String result = r.request().delete(String.class);
		return result;
	}
}
