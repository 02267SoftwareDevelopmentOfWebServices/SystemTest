package systemtest;

import java.util.List;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

/**
 * Customer Mobile simulator
 * 
 * @author Anna Nguyen s154230
 *
 */

public class CustomerMobile {
	Client client = ClientBuilder.newClient();
	WebTarget r;
	List<Token> tokens;
	String cpr;
	String firstName;
	String lastName;

	public CustomerMobile(String cpr, String firstName, String lastName) {
		this.cpr = cpr;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	/**
	 * This method simulates that the customer connects to their bank account.
	 * 
	 * @return the string "true" if the customer now has a bank account
	 */
	public String connectAccount() {
		r = client.target("http://02267-istanbul.compute.dtu.dk:8081/accounts");
		// Simulates the customer inputting personal information
		JSONObject info = new JSONObject();
		info.put("firstName", firstName);
		info.put("lastName", lastName);
		info.put("cpr", cpr);
		info.put("type", "customer");

		String message = info.toString();

		String result = r.request().post(Entity.entity(message,"text/plain"),String.class);
		return result;
	}

	/**
	 * This method simulates that the customer requests an amount of tokens.
	 * 
	 * @param amount is the number of tokens to be requested
	 * @return a JSONarray with the tokens
	 */
	public List<Token> requestToken(int amount) {
		r = client.target("http://02267-istanbul.compute.dtu.dk:8082/tokens?amount="+amount);

		Response response = r.request().post(Entity.entity(cpr,"text/plain"),Response.class);

		tokens = response.readEntity(new GenericType<List<Token>>() {});

		return tokens;
	}

	/**
	 * This method retrieves the number of unused tokens of a customer.
	 * 
	 * @return a string indicating the number of unused tokens
	 */
	public String numberOfUnusedTokens() {
		r = client.target("http://02267-istanbul.compute.dtu.dk:8082/tokens?customerid="+cpr);
		String result = r.request().get(String.class);

		return result;
	}

	/**
	 * This method simulates that the customer changes their bank account name
	 * 
	 * @return the string "true" if the change succeeds
	 */
	public String editAccountName(String newName) {
		r = client.target("http://02267-istanbul.compute.dtu.dk:8081/accounts/"+cpr);

		String result = r.request().put(Entity.entity(newName,"text/plain"),String.class);
		return result;
	}

	/**
	 * This method simulates that the customer disconnects their bank account
	 * 
	 * @return the string "true" if disconnected
	 */
	public String disconnectAccount() {
		r = client.target("http://02267-istanbul.compute.dtu.dk:8081/accounts/"+cpr);

		String result = r.request().delete(String.class);
		return result;
	}

	/**
	 * This method clears the database of token manager
	 */

	public void clearDatabase() {
		r = client.target("http://02267-istanbul.compute.dtu.dk:8082/tokens");
		r.request().put(null, String.class);

	}
}
