package systemtest;

import static org.junit.Assert.*;

import java.util.List;

import org.json.JSONArray;
import org.junit.runner.RunWith;

import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * This class contains step definitions for the system tests.
 * The system tests are based on cucumber.
 * See .features-file for the scenarios.
 * 
 * @author Anna Nguyen s154230
 *
 */

public class SystemTest {

	List<Token> tokens;
	String errorMessage;
	CustomerMobile cMobile;
	MerchantMobile mMobile;
	String result;
	
	String cpr;

	@After
	public void tearDown() throws Exception {
		// Delete dtupay account and bank account
		cMobile.clearDatabase();
		cMobile.disconnectAccount();
	}
	
	@Given("^that I am a customer with a name and CPR number$")
	public void thatIHaveACPRNumber() throws Throwable {
		cMobile = new CustomerMobile("223456-7890", "Pieter", "Hansen");
	}

	@Given("^that DTUPay is connected to my bank account$")
	public void thatDTUPayIsConnectedToMyBankAccount() throws Throwable {
		cMobile = new CustomerMobile("323456-7890", "Poot", "Prius");
		cMobile.connectAccount();
	}
	
	@Given("^that DTUPay is connected to a merchant's bank account$")
	public void thatDTUPayIsConnectedToAMerchantSBankAccount() throws Throwable {
		mMobile = new MerchantMobile("212365-6789", "Sumba", "Gerda");
		mMobile.connectAccount();
	}
	
	@Given("^that I have (\\d+) tokens$")
	public void thatIHaveTokens(int arg1) throws Throwable {
		//cMobile.connectAccount();
		tokens = cMobile.requestToken(arg1);
	}

	@When("^I provide appropriate information to DTUPay$")
	public void iProvideAppropriateInformationToDTUPay() throws Throwable {
		result = cMobile.connectAccount();
	}

	@When("^I request (\\d+) token$")
	public void iRequestToken(int arg1) throws Throwable {
		tokens = cMobile.requestToken(arg1);
	}

	@When("^a merchant scans one token$")
	public void aMerchantScansMyToken() throws Throwable {
		// Get one token from customer
		//String tokenId = cMobile.getOneTokenId();
		String tokenId = tokens.get(0).getString();

		result = mMobile.scanToken(tokenId, 0);
	}

	@When("^I change my name$")
	public void iChangeMyName() throws Throwable {
		result = cMobile.editAccountName("Peder");
	}

	@Then("^I have (\\d+) unused tokens$")
	public void iHaveUnusedTokens(int arg1) throws Throwable {
		result = cMobile.numberOfUnusedTokens();
		assertEquals(""+arg1, result);
	}

	@Then("^my name is changed in DTUPay$")
	public void myNameIsChangedInDTUPay() throws Throwable {
		assertEquals("Peder", result);
	}
	
	@Then("^100 kroner are transferred from my account to the merchant's$")
	public void kronerAreTransferredFromMyAccountToTheMerchantS() throws Throwable {
		mMobile.disconnectAccount();
		assertEquals("Transfer true", result);
	}
	
	@Then("^DTUPay is connected to my bank account$")
	public void dtupayIsConnectedToMyBankAccount() throws Throwable {
		assertEquals("true", result);
	}
}
