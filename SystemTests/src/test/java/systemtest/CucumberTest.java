package systemtest;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;

/**
 * Cucumber test runner
 * 
 * @author Ole Eilgaard s154086
 *
 */

@RunWith(Cucumber.class)
@CucumberOptions(features="features",
		snippets=SnippetType.CAMELCASE)
public class CucumberTest {
}
