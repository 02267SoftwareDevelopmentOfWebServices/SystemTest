 	Feature: System features
 		
 		Scenario: Connect DTUPay to bank account
 		Given that I am a customer with a name and CPR number
 		When I provide appropriate information to DTUPay
 		Then DTUPay is connected to my bank account
 		
 		Scenario: Request token
 		Given that DTUPay is connected to my bank account
 		When I request 1 token
 		Then I have 1 unused tokens
 		
 		Scenario: Payment
 		Given that DTUPay is connected to my bank account
 		And that I have 7 tokens
 		And that DTUPay is connected to a merchant's bank account
 		When a merchant scans one token
 		Then 100 kroner are transferred from my account to the merchant's
 		And I have 0 unused tokens
 	
 		Scenario: Edit account information
 		Given that DTUPay is connected to my bank account
 		When I change my name
 		Then my name is changed in DTUPay